(TeX-add-style-hook
 "alggeoI_main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("inputenc" "utf8") ("fontenc" "T1") ("setspace" "onehalfspacing") ("datetime2" "useregional" "showdow")))
   (TeX-run-style-hooks
    "latex2e"
    "alggeoI_2018-10-11"
    "alggeoI_2018-10-15"
    "article"
    "art10"
    "babel"
    "inputenc"
    "fontenc"
    "lmodern"
    "amsmath"
    "amssymb"
    "amsthm"
    "fancyhdr"
    "enumitem"
    "geometry"
    "cleveref"
    "setspace"
    "pgf"
    "tikz"
    "tikz-cd"
    "mathrsfs"
    "stmaryrd"
    "titlesec"
    "datetime2")
   (TeX-add-symbols
    '("orbit" 2)
    "C"
    "R"
    "N"
    "Z"
    "Q"
    "sectionbreak")
   (LaTeX-add-amsthm-newtheorems
    "lemma"
    "theorem"
    "proposition"
    "corollary"
    "definition"
    "example"
    "note"
    "remark"))
 :latex)

