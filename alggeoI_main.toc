\babel@toc {english}{}
\contentsline {section}{\numberline {0}Preliminaries}{3}{section.0}
\contentsline {section}{\numberline {1}Varieties}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Affine Varieties}{4}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Morphisms}{8}{subsubsection.1.1.1}
\contentsline {subsection}{\numberline {1.2}Projective varieties}{10}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Morphisms}{11}{subsubsection.1.2.1}
\contentsline {section}{\numberline {2}Sheaves}{12}{section.2}
\contentsline {subsection}{\numberline {2.1}Morphism of presheaves}{12}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Further topics}{13}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Structure sheaf}{14}{subsection.2.3}
